package com.eazybytes.eazyschool.config;

import org.springframework.stereotype.Component;

@Component
public class MySingletonBean {
    private String testAttribute;

    public String getTestAttribute() {
        return testAttribute;
    }

    public void setTestAttribute(String testAttribute) {
        this.testAttribute = testAttribute;
    }
}
